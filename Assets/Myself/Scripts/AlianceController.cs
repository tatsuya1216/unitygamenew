﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using System.Collections.Generic;

public class AlianceController : MonoBehaviour {

	public LayerMask targetlayer;
	public LayerMask attackinglayer;
	public LayerMask blockinglayer;
	public int Ahp;
	public int Power;
	public bool IsAttacking = false;
	private float speed = 1;
	private float moveCount = 0.0f;
	private List <Vector3> TargetPosition = new List<Vector3> ();
	private Rigidbody2D rb2D;
	private ActionController actionController;
	private Animator animator;
	private EnemyController enemyScript;
	public AudioClip attackSound;

	public IEnumerator AlianceMove (){
		
		TargetPosition.Clear ();
		actionController = GetComponent<ActionController> ();
		rb2D = GetComponent<Rigidbody2D> ();
		animator = this.GetComponent<Animator> ();
		int rnd = 0;


		while (true) {

			if (Ahp <= 0) {
				Destroy (gameObject);
				yield break;
			}

			if (moveCount <= 0.0f) {
				
				TargetPosition.Clear ();
				Vector3 start = transform.position;

//////////////////////敵との戦闘//////////////////////
				for (int n = 0; n < 5; n++) {

					RaycastHit2D r = Physics2D.Linecast (start, new Vector2 (start.x + (5 - n), start.y), blockinglayer);
					RaycastHit2D l = Physics2D.Linecast (start, new Vector2 (start.x + (5 - n), start.y), targetlayer);
					if (r.collider == null) {
						if (l.collider != null) {
							l = Physics2D.Linecast (start, new Vector2 (start.x + 1.0f, start.y), attackinglayer);
							if (l.collider == null) {
								TargetPosition.Add (new Vector3 (start.x + 1, start.y, 0));
								rnd = 0;
								speed = 4;
								break;
							}
							if (l.collider != null) {
								IsAttacking = true;
								enemyScript = l.collider.GetComponentInParent<EnemyController> ();
								rnd = 0;
								TargetPosition.Add (start);
								animator.SetTrigger ("Attack");	
								SoundManager.instance.PlaySingle (attackSound);
								enemyScript.Ehp -= Power;
								if (enemyScript.Ehp <= 0) {
									IsAttacking = false;
								}
								break;
							}
						}
					}

					r = Physics2D.Linecast (start, new Vector2 (start.x - (5 - n), start.y), blockinglayer);
					l = Physics2D.Linecast (start, new Vector2 (start.x - (5 - n), start.y), targetlayer);
					if (r.collider == null) {
						if (l.collider != null) {
							l = Physics2D.Linecast (start, new Vector2 (start.x - 1.0f, start.y), attackinglayer);
							if (l.collider == null) {
								TargetPosition.Add (new Vector3 (start.x - 1, start.y, 0));
								rnd = 0;
								speed = 4;
								break;
							}
							if (l.collider!=null) {
								IsAttacking = true;
								enemyScript = l.collider.GetComponentInParent<EnemyController> ();
								TargetPosition.Clear ();
								rnd = 0;
								TargetPosition.Add (start);
								animator.SetTrigger ("Attack");
								SoundManager.instance.PlaySingle (attackSound);
								enemyScript.Ehp -= Power;
								if (enemyScript.Ehp <= 0) {
									IsAttacking = false;
								}
								break;
							}
						}
					}

					r = Physics2D.Linecast (start, new Vector2 (start.x, start.y + (5 - n)), blockinglayer);
					l = Physics2D.Linecast (start, new Vector2 (start.x, start.y + (5 - n)), targetlayer);
					if (r.collider == null) {
						if (l.collider != null) {
							l = Physics2D.Linecast (start, new Vector2 (start.x, start.y + 1.0f), attackinglayer);
							if (l.collider == null) {
								TargetPosition.Add (new Vector3 (start.x, start.y + 1, 0));
								rnd = 0;
								speed = 4;
								break;
							}
							if (l.collider!=null) {
								IsAttacking = true;
								enemyScript = l.collider.GetComponentInParent<EnemyController> ();
								TargetPosition.Clear ();
								rnd = 0;
								TargetPosition.Add (start);
								animator.SetTrigger ("Attack");
								SoundManager.instance.PlaySingle (attackSound);
								enemyScript.Ehp -= Power;
								if (enemyScript.Ehp <= 0) {
									IsAttacking = false;
								}
								break;
							}
						}
					}

					r = Physics2D.Linecast (start, new Vector2 (start.x, start.y - (5 - n)), blockinglayer);
					l = Physics2D.Linecast (start, new Vector2 (start.x, start.y - (5 - n)), targetlayer);
					if (r.collider == null) {
						if (l.collider != null) {
							l = Physics2D.Linecast (start, new Vector2 (start.x, start.y - 1.0f), attackinglayer);
							if (l.collider == null) {
								TargetPosition.Add (new Vector3 (start.x, start.y - 1, 0));
								rnd = 0;
								speed = 4;
								break;
							}
							if (l.collider!=null) {
								IsAttacking = true;
								enemyScript = l.collider.GetComponentInParent<EnemyController> ();
								TargetPosition.Clear ();
								rnd = 0;
								TargetPosition.Add (start);
								animator.SetTrigger ("Attack");
								SoundManager.instance.PlaySingle (attackSound);
								enemyScript.Ehp -= Power;
								if (enemyScript.Ehp <= 0) {
									IsAttacking = false;
								}
								break;
							}
						}
					}
				}

				if (TargetPosition.Count == 0) {

					float x = Mathf.Round (rb2D.position.x);
					float y = Mathf.Round (rb2D.position.y);

					rb2D.position = new Vector2 (x, y);

					TargetPosition = actionController.Search ();
					rnd = Random.Range (0, TargetPosition.Count);

					speed = 0.5f;
				}

				if (IsAttacking) {
					RaycastHit2D k = Physics2D.Linecast (transform.position, new Vector2 (transform.position.x + 1, transform.position.y), targetlayer);
					if (k.collider != null) {
						var diff = (new Vector3(transform.position.x + 1, transform.position.y, 0f) - transform.position).normalized;
						transform.rotation = Quaternion.FromToRotation (Vector3.right, diff);
					}
					k = Physics2D.Linecast (transform.position, new Vector2 (transform.position.x - 1, transform.position.y), targetlayer);
					if (k.collider != null) {
						var diff = (new Vector3(transform.position.x - 1, transform.position.y, 0f) - transform.position).normalized;
						transform.rotation = Quaternion.FromToRotation (Vector3.right, diff);
					}
				} else if (TargetPosition [rnd].y == start.y && TargetPosition [rnd] != start) {
					var diff = (TargetPosition [rnd] - transform.position).normalized;
					transform.rotation = Quaternion.FromToRotation (Vector3.right, diff);
				}
					
				moveCount = 1.0f;
			}

			moveCount -= Time.deltaTime * speed;
			transform.position = Vector3.MoveTowards (rb2D.position, TargetPosition [rnd], speed * Time.deltaTime);

			yield return null;
		}
	}

	// Use this for initialization
	void Start () {
	}
		
	// Update is called once per frame
	void Update () {
		
	}
}