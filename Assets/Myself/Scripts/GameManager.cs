﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	[SerializeField] Dungeon dungeonScript = null;
	[SerializeField] TimeController timeScript = null;
	private PlayerController playerScript;
	public GameObject Enemy;
	private GameObject g;
	private EnemyController enemyController;
	public List<GameObject> Enemies = new List<GameObject> ();
	private float count = 0;
	private int rnd1;
	private int rnd2;
	private int[] index = new int[] { 1, 2, 3, 4 };
	public float setTime;
	private int a;
	private int enemyCount = 0;
	public int maxEnemy;
	public GameObject gameOverText;
	public GameObject gameClearText;
	private float timecount = 0;
	private bool gameStop = false;
	float t;
	private float timeCount = 0.0f;
	public float instantiateSpan = 1.0f;
	bool isInstantiate = false;

	// Use this for initialization
	void Start () {
		dungeonScript.SetMap ();
		SoundManager.instance.musicSource.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		playerScript = GetComponent<PlayerController> ();
		if (playerScript.SetTreasure () == false) {
			playerScript.SetTreasure ();
		}
		if (playerScript.SetTreasure () == true) {
			count += Time.deltaTime;
			if (count < 1) {
				FadeManager.Instance.Fade (0.4f);
			}
			if (1 <= count && count < setTime) {
				playerScript.SetCharacters ();
			}
			if (setTime <= count && count < setTime + 1) {
				FadeManager.Instance.Fade (0.4f);
			}
			if (count >= setTime + 1 && !isInstantiate) {
				isInstantiate = true;
				StartCoroutine (enemyInstantiate ());
  			}
			if (isInstantiate && enemyCount > 0 && !gameStop) {
				timeScript.TimeCount (gameStop);
				if (Enemies.Count > 0) {
					for (int n = 0; n < Enemies.Count; n++) {
						enemyController = Enemies [n].GetComponent<EnemyController> ();
						if (enemyController.GameOver ()) {
							gameStop = true;
							timeScript.TimeCount (gameStop);
							gameOverText.SetActive (true);
							SoundManager.instance.musicSource.Stop ();
							BackToTitle ();
						}
					}
				}
				if (!timeScript.TimeCount (gameStop)) {
					timeScript.time = 0;
					gameClearText.SetActive (true);
					BackToTitle ();
				}
				if (Enemies.Count == 0) {
					gameStop = true;
					timeScript.TimeCount (gameStop);
					gameClearText.SetActive (true);
					BackToTitle ();
				}
			}
		}
	}

	public IEnumerator enemyInstantiate () {
		while (enemyCount < maxEnemy) {
		
			timeCount += Time.deltaTime;
			if (timeCount >= instantiateSpan) {
				timeCount = 0.0f;
				for (int num = 0; num < 10; num++) {
					rnd1 = Random.Range (0, 4);
					a = index [rnd1];
					rnd2 = Random.Range (0, 4);
					index [rnd1] = index [rnd2];
					index [rnd2] = a;
				}

				g = Instantiate (Enemy, new Vector3 (1, 5, 0), Quaternion.identity) as GameObject;
				Enemies.Add (g);
				EnemyA e = g.GetComponent<EnemyA> ();
				e.ue = index [0];
				e.shita = index [1];
				e.migi = index [2];
				e.hidari = index [3];
				e.AMove (this);

				enemyCount++;
			}
			yield return null;
		}

		if (enemyCount == maxEnemy) {
			yield break;
		}
	}

	public void BackToTitle () {
		timecount += Time.deltaTime;

		if (timecount > 3) {
			FadeManager.Instance.LoadLevel ("Title", 0.2f);
			SoundManager.instance.musicSource.Stop ();
			timecount = 0;
		}
	}

	public void DestroyEnemy(GameObject g)
	{
		Enemies.Remove (g);
		DestroyObject (g);
	}
}
