﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Dungeon : MonoBehaviour {

	public static List <Vector3> gridPositions = new List<Vector3> ();
	public static List <Vector3> roadPositions = new List<Vector3> ();
	public static List <Vector3> movePoints = new List<Vector3> ();
	public static List <Vector3> backPoints = new List<Vector3> ();
	public int columns;
	public int rows;
	public GameObject[] Tiles;

	private int[,] map = new int[,] {
		{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 },
		{ 2, 3, 3, 1, 1, 3, 0, 0, 3, 1, 1, 3, 0, 0, 3, 0, 0, 3, 2 },
		{ 2, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 2 },
		{ 2, 1, 3, 0, 0, 3, 0, 0, 3, 1, 1, 0, 1, 1, 0, 1, 1, 3, 2 },
		{ 2, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 2 },
		{ 2, 3, 4, 0, 0, 0, 4, 0, 4, 0, 4, 4, 1, 1, 3, 0, 0, 3, 2 },
		{ 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 2 },
		{ 2, 1, 3, 1, 3, 1, 3, 0, 0, 3, 3, 3, 0, 0, 3, 0, 0, 3, 2 },
		{ 2, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 2 },
		{ 2, 3, 3, 0, 3, 0, 3, 1, 1, 3, 1, 3, 0, 0, 3, 1, 1, 3, 2 },
		{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }
	};




	public void SetMap () {
		roadPositions.Clear ();
		for (int y = 0; y < columns; y++) {
			for (int x = 0; x < rows; x++) {
				
				gridPositions.Add (new Vector3 (x, y, 0f));
				GameObject tiles = Tiles [map[columns - y - 1, x]];
				Instantiate (tiles, new Vector3 (x, y, 0f), Quaternion.identity);

				if (map [columns - y - 1, x] == 0 || map [columns - y - 1, x] == 3 || map [columns - y - 1, x] == 4) {
					roadPositions.Add (new Vector3 (x, y, 0f));
				}
				if (map [columns - y - 1, x] == 3 || map [columns - y - 1, x] == 4) {
					movePoints.Add (new Vector3 (x, y, 0f));
				}
				if (map [columns - y - 1, x] == 4) {
					backPoints.Add (new Vector3 (x, y, 0f));
				}
			}
		}
	}



	// Use this for initialization
	void Start () {
		
	}


	// Update is called once per frame
	void Update () {
	}
}




	/*for (int i = 0, iMax = mapInfo.GetLength (0); i < iMax; i++) {
		for (int j = 0, jMax = mapInfo.GetLength (1); j < jMax; j++) {
			Debug.Log (Tiles [mapInfo [i, j]].name);
		}
	}
	*/

	// ゲームに使用する範囲を指定
	/* 
	private List <Vector3> outerwalls = new List<Vector3> ();
	
	void InitialiseList ()
	{
		gridPositions.Clear ();
		for (int x = -9; x < columns; x++){
			for (int y = -5; y < rows; y++){
				if (x == -9 || x == 9 || y == -5 || y == 5) {
					outerwalls.Add (new Vector3 (x, y, 0f));
				} else {
					gridPositions.Add (new Vector3 (x, y, 0f));
				}
			}
		}
	}
	*/

//タイルの自動配置（マップを固定にしたため削除）
/*void SetTiles (int n) {
		for (int x = 0; x < gridPositions.Count; x++) {
			if (n == 2) {
				Instantiate (Tiles [n], outerwalls [x], Quaternion.identity);
			} else {
				Instantiate (Tiles [n], gridPositions [x], Quaternion.identity);
			}
		}
	}
	void SetTiles (int n, List<Vector3> vec3List) {
		for (int x = 0; x < vec3List.Count; x++) {
			Instantiate (Tiles [n], vec3List [x], Quaternion.identity);
		}
	}
		
	void SetBlocks (int n) {
		for(int x = -7; x < 8; x++){
			for(int num = 0; num < 4; num++){
				int y = Random.Range (-4, 5);
				Instantiate (Tiles [n], new Vector3 (x, y, 0), Quaternion.identity);
				InnerWallPosition.Add (new Vector3 (x, y, 0));
			}
		}
	}
	*/
