﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class EnemyA : MonoBehaviour {

	//上から操作する敵

	[SerializeField]
	private EnemyController enemyScript = null;
	public float speed;
	public int ue;
	public int shita;
	public int migi;
	public int hidari;

	public void AMove( GameManager gm ) {
		enemyScript.gm = gm;
		StartCoroutine (enemyScript.EnemyMove (speed, ue, shita, migi, hidari));
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
