﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class EnemyController : MonoBehaviour {

	public int Ehp;
	public int Power;
	public LayerMask blockinglayer;
	public LayerMask treasurelayer;
	public LayerMask targetlayer;
	//public GameObject treasure;
	private AlianceController alianceScript;
	private bool IsTreasureHolding = false;
	public GameManager gm = null;
	public AudioClip getSound;

	//キャラクターの移動
	public IEnumerator EnemyMove (float speed, int ue, int shita, int migi, int hidari){

		float distance = 0;
		int number = 0;
		Rigidbody2D rb2D;
		Transform trans;
		Vector3 currentPoint;
		Vector3 ueTarget;
		Vector3 shitaTarget;
		Vector3 migiTarget;
		Vector3 hidariTarget;
		Vector3 targetPoint = new Vector3 (0, 0, 0);
		List <Vector3> ashiato = new List<Vector3> ();
		List <Vector3> movePoints = new List<Vector3> (Dungeon.movePoints);
		Animator animator;
		Vector2 treasurePosition;

		rb2D = GetComponent<Rigidbody2D> ();
		animator = this.GetComponent<Animator> ();

		while (true) {

			if (Ehp <= 0) {
				gm.DestroyEnemy(gameObject);
				yield break;
			}

			bool IsAttacking = false;

			//周囲に敵がいれば戦う

			for (int n = 0; n < 3; n++) {

				currentPoint = rb2D.position;

				int x = (int)Mathf.Round (currentPoint.x);
				int y = (int)Mathf.Round (currentPoint.y);

				RaycastHit2D r = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x + (3 - n), currentPoint.y), blockinglayer);
				RaycastHit2D l = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x + (3 - n), currentPoint.y), targetlayer);
				RaycastHit2D k = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x + 1, currentPoint.y), targetlayer);
				if (r.collider == null && l.collider != null && k.collider == null) {
					IsAttacking = true;
					targetPoint = new Vector3 (x, y, 0);
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					if (alianceScript.IsAttacking == true && k.collider == null) {
						targetPoint = new Vector3 (x + 1, y, 0);
					}
				}
				if (k.collider != null) {
					IsAttacking = true;
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					animator.SetTrigger ("Attack");
					alianceScript.Ahp -= Power;
					targetPoint = currentPoint;
					break;
				}

				r = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x - (3 - n), currentPoint.y), blockinglayer);
				l = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x - (3 - n), currentPoint.y), targetlayer);
				k = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x - 1, currentPoint.y), targetlayer);
				if (r.collider == null && l.collider != null && k.collider == null) {
					IsAttacking = true;
					targetPoint = new Vector3 (x, y, 0);
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					if (alianceScript.IsAttacking == true && k.collider == null) {
						targetPoint = new Vector3 (x - 1, y, 0);
					}
				}
				if (k.collider != null) {
					IsAttacking = true;
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					animator.SetTrigger ("Attack");
					alianceScript.Ahp -= Power;
					targetPoint = currentPoint;
					break;
				}

				r = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x, currentPoint.y + (3 - n)), blockinglayer);
				l = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x, currentPoint.y + (3 - n)), targetlayer);
				k = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x, currentPoint.y + 1), targetlayer);
				if (r.collider == null && l.collider != null) {
					IsAttacking = true;
					targetPoint = new Vector3 (x, y, 0);
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					if (alianceScript.IsAttacking == true && k.collider == null) {
						targetPoint = new Vector3 (x, y + 1, 0);
					}
				}
				if (k.collider != null) {
					IsAttacking = true;
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					animator.SetTrigger ("Attack");
					alianceScript.Ahp -= Power;
					targetPoint = currentPoint;
					break;
				}

				r = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x, currentPoint.y - (3 - n)), blockinglayer);
				l = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x, currentPoint.y - (3 - n)), targetlayer);
				k = Physics2D.Linecast (currentPoint, new Vector2 (currentPoint.x, currentPoint.y - 1), targetlayer);
				if (r.collider == null && l.collider != null) {
					IsAttacking = true;
					targetPoint = new Vector3 (x, y, 0);
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					if (alianceScript.IsAttacking == true && k.collider == null) {
						targetPoint = new Vector3 (x, y - 1, 0);
					}
				}
				if (k.collider != null) {
					IsAttacking = true;
					alianceScript = l.collider.GetComponentInParent<AlianceController> ();
					animator.SetTrigger ("Attack");
					alianceScript.Ahp -= Power;
					targetPoint = currentPoint;
					break;
				}
			}

			if (distance == 0f) {

				if (PlayerController.Treasure.activeSelf == true) {
					if (IsAttacking == false) {
						trans = PlayerController.Treasure.GetComponent<Transform> ();
						treasurePosition = (Vector2)trans.position;
						int a = 0;
						int b = 0;
						int c = 0;
						int d = 0;

						currentPoint = rb2D.position;

						int x = (int)Mathf.Round (currentPoint.x);
						int y = (int)Mathf.Round (currentPoint.y);

						ueTarget = new Vector3 (x, y + 20, 0);
						shitaTarget = new Vector3 (x, y - 20, 0);
						migiTarget = new Vector3 (x + 20, y, 0);
						hidariTarget = new Vector3 (x - 20, y, 0);

						if (currentPoint == new Vector3 (1.0f, 5.0f, 0f)) {
							movePoints = new List<Vector3> (Dungeon.movePoints);
							ashiato.Clear ();
							yield return null;
						}

						for (int n = 0; n < movePoints.Count; n++) {
							if (currentPoint == movePoints [n]) {
								ashiato.Add (currentPoint);						//足跡に現在地を登録
								movePoints.RemoveAt (n);				//現在地がどこかの点と一致していればリストからその点を削除
							}
						}

						for (int n = 0; n < movePoints.Count; n++) {
							RaycastHit2D r = Physics2D.Linecast (currentPoint, movePoints [n], blockinglayer);
							if (x == movePoints [n].x) {
								if (r.collider == null) {						//目標地点と現在地の間に何もなければtargetPointに追加

									if (movePoints [n].y > y && ueTarget.y > movePoints [n].y) {
										ueTarget = movePoints [n];
										a = (5 - ue) * (5 - ue) * (5 - ue);
									}

									if (movePoints [n].y < y && shitaTarget.y < movePoints [n].y) {
										shitaTarget = movePoints [n];
										b = (5 - shita) * (5 - shita) * (5 - shita);
									}

								}
							}

							if (y == movePoints [n].y) {
								if (r.collider == null) {						//目標地点と現在地の間に何もなければtargetPointに追加

									if (movePoints [n].x > x && migiTarget.x > movePoints [n].x) {
										migiTarget = movePoints [n];
										c = (5 - migi) * (5 - migi) * (5 - migi);
									}

									if (movePoints [n].x < x && hidariTarget.x < movePoints [n].x) {
										hidariTarget = movePoints [n];
										d = (5 - hidari) * (5 - hidari) * (5 - hidari);
									}
								}
							}
						}

						if (a == 0 && b == 0 && c == 0 && d == 0) {

							int count = 0;
							for (int m = 0; m < ashiato.Count - 1; m++) {
						
								RaycastHit2D r = Physics2D.Linecast (currentPoint, ashiato [m], blockinglayer);

								if (r.collider == null) {
							
									if (x == ashiato [m].x && currentPoint != ashiato [m]) {
										if (y < ashiato [m].y && ueTarget.y > ashiato [m].y) {
											ueTarget = ashiato [m];
											a = (5 - ue) * (5 - ue) * (5 - ue);
										}
										if (y > ashiato [m].y && shitaTarget.y < ashiato [m].y) {
											shitaTarget = ashiato [m];
											b = (5 - shita) * (5 - shita) * (5 - shita);
										}
									}
									if (y == ashiato [m].y && currentPoint != ashiato [m]) {
										if (x < ashiato [m].x && migiTarget.x > ashiato [m].x) {
											migiTarget = ashiato [m];
											c = (5 - migi) * (5 - migi) * (5 - migi);
										}
										if (x > ashiato [m].x && hidariTarget.x < ashiato [m].x) {
											hidariTarget = ashiato [m];
											d = (5 - hidari) * (5 - hidari) * (5 - hidari);
										}
									}
								}
							}

							if (a != 0) {
								count++;
							}
							if (b != 0) {
								count++;
							}
							if (c != 0) {
								count++;
							}
							if (d != 0) {
								count++;
							}

							if (count == 1) {
								ashiato.RemoveAt (ashiato.Count - 1);
							} 

						}

						if (a == 0 && b == 0 && c == 0 && d == 0) {
							movePoints = new List<Vector3> (Dungeon.movePoints);
							ashiato.Clear ();
							yield return null;
						} else {
							int e = 100 * a / (a + b + c + d);
							int f = 100 * b / (a + b + c + d);
							int g = 100 * c / (a + b + c + d);
							int h = 100 * d / (a + b + c + d);

							int num = Random.Range (0, 100);
							if (num >= 100 - e && num < 100) {
								targetPoint = ueTarget;
							}
							if (num >= 100 - e - f && num < 100 - e) {
								targetPoint = shitaTarget;
							}
							if (num >= 100 - e - f - g && num < 100 - e - f) {
								targetPoint = migiTarget;
							}
							if (num >= 100 - e - f - g - h && num < 100 - e - f - g) {
								targetPoint = hidariTarget;
							}
						}
				
					
						//宝があれば優先的に宝の場所に向かう
						if ((Vector2)currentPoint != treasurePosition) {

							for (int n = 0; n < 5; n++) {

								RaycastHit2D r1 = Physics2D.Linecast (currentPoint, new Vector2 (x + (5 - n), y), blockinglayer);
								RaycastHit2D l1 = Physics2D.Linecast (currentPoint, new Vector2 (x + (5 - n), y), treasurelayer);
								RaycastHit2D k1 = Physics2D.Linecast (currentPoint, new Vector2 (x + (5 - n), y), targetlayer);
								if (r1.collider == null && k1.collider == null) {
									if (l1.collider) {
										targetPoint = treasurePosition;
									}
								}

								r1 = Physics2D.Linecast (currentPoint, new Vector2 (x - (5 - n), y), blockinglayer);
								l1 = Physics2D.Linecast (currentPoint, new Vector2 (x - (5 - n), y), treasurelayer);
								k1 = Physics2D.Linecast (currentPoint, new Vector2 (x + (5 - n), y), targetlayer);
								if (r1.collider == null && k1.collider == null) {
									if (l1.collider) {
										targetPoint = treasurePosition;
									}
								}

								r1 = Physics2D.Linecast (currentPoint, new Vector2 (x, y + (5 - n)), blockinglayer);
								l1 = Physics2D.Linecast (currentPoint, new Vector2 (x, y + (5 - n)), treasurelayer);
								k1 = Physics2D.Linecast (currentPoint, new Vector2 (x + (5 - n), y), targetlayer);
								if (r1.collider == null && k1.collider == null) {
									if (l1.collider) {
										targetPoint = treasurePosition;
									}
								}

								r1 = Physics2D.Linecast (currentPoint, new Vector2 (x, y - (5 - n)), blockinglayer);
								l1 = Physics2D.Linecast (currentPoint, new Vector2 (x, y - (5 - n)), treasurelayer);
								k1 = Physics2D.Linecast (currentPoint, new Vector2 (x + (5 - n), y), targetlayer);
								if (r1.collider == null && k1.collider == null) {
									if (l1.collider) {
										targetPoint = treasurePosition;
									}
								}
							}
						}

						if ((Vector2)currentPoint == treasurePosition) {
							PlayerController.Treasure.SetActive (false);
							IsTreasureHolding = true;
							SoundManager.instance.PlaySingle (getSound);
						}
					}
				}

				if (PlayerController.Treasure.activeSelf == false){
					if (IsAttacking == false) {

						RaycastHit2D r = Physics2D.Linecast (rb2D.position, new Vector3 (1, 5, 0), blockinglayer);
						if (r.collider == null) {
							targetPoint = new Vector3 (1, 5, 0);
						} else {
							for (int n = 0; n < Dungeon.backPoints.Count; n++) {
								r = Physics2D.Linecast (rb2D.position, Dungeon.backPoints [n], blockinglayer);
								if (rb2D.position.x == Dungeon.backPoints [n].x) {

									if (r.collider == null) {
										targetPoint = Dungeon.backPoints [n];
										break;
									}
								}
								if (r.collider != null) {
									r = Physics2D.Linecast (rb2D.position, ashiato [ashiato.Count - 1 - number], blockinglayer);
									if (r.collider != null || rb2D.position == (Vector2)ashiato [ashiato.Count - 1 - number]) {
										number++;
									}
									targetPoint = ashiato [ashiato.Count - 1 - number];
								}
							}
						}
					}
				}

				if (targetPoint == transform.position) {
					RaycastHit2D k = Physics2D.Linecast (transform.position, new Vector2 (transform.position.x + 1, transform.position.y), targetlayer);
					if (k.collider != null) {
						var diff = (new Vector3(transform.position.x + 1, transform.position.y, 0f) - transform.position).normalized;
						transform.rotation = Quaternion.FromToRotation (Vector3.right, diff);
					}
					k = Physics2D.Linecast (transform.position, new Vector2 (transform.position.x - 1, transform.position.y), targetlayer);
					if (k.collider != null) {
						var diff = (new Vector3(transform.position.x - 1, transform.position.y, 0f) - transform.position).normalized;
						transform.rotation = Quaternion.FromToRotation (Vector3.right, diff);
					}
				} else if (targetPoint.y == transform.position.y) {
					var diff = (targetPoint - transform.position).normalized;
					transform.rotation = Quaternion.FromToRotation (Vector3.right, diff);
				}
			}
				
			distance = Vector3.Distance(rb2D.position, targetPoint);
			Vector3 newPosition = Vector3.MoveTowards (rb2D.position, targetPoint, speed * Time.deltaTime);
			rb2D.MovePosition (newPosition);
			yield return null;
		}
	} 

	public bool GameOver () {
		if (IsTreasureHolding) {
			if (this.transform.position == new Vector3 (1.0f, 5.0f, 0f)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		} 
	}
		
	void Start () {
	
	}

	void Update () {
	}
}

