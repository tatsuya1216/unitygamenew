﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionController : MonoBehaviour {

	public LayerMask blockinglayer;

	//周囲4マスに壁があるか調べる
	public List<Vector3> Search () {

		List <Vector3> TargetPosition = new List<Vector3> ();
		TargetPosition.Clear ();

		Vector3 start = transform.position;
		RaycastHit2D r = Physics2D.Linecast (start, new Vector2 (start.x + 1, start.y), blockinglayer);
		if (r.collider==null) {
			TargetPosition.Add (new Vector3 (start.x + 1, start.y, 0f));
		}

		r = Physics2D.Linecast (start, new Vector2 (start.x - 1, start.y), blockinglayer);
		if (r.collider==null) {
			TargetPosition.Add (new Vector3 (start.x - 1, start.y, 0f));
		}

		r = Physics2D.Linecast (start, new Vector2 (start.x, start.y + 1), blockinglayer);
		if (r.collider==null) {
			TargetPosition.Add (new Vector3 (start.x, start.y + 1, 0f));
		}

		r = Physics2D.Linecast (start, new Vector2 (start.x, start.y - 1), blockinglayer);
		if (r.collider==null) {
			TargetPosition.Add (new Vector3 (start.x, start.y - 1, 0f));
		}

		return TargetPosition;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
