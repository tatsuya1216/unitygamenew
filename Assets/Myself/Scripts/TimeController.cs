﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TimeController : MonoBehaviour {

	public float time;

	public bool TimeCount (bool gameStop) {
		GetComponent<Text> ().text = ((int)time).ToString ();

		if (!gameStop) {
			time -= 0.5f * Time.deltaTime;
			if (time < 0) {
				time = 0;
				GetComponent<Text> ().text = ((int)time).ToString ();
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
}
