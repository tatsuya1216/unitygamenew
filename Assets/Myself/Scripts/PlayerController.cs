﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

	public GameObject Aliance;
	public static GameObject Treasure = null;
	public GameObject treasure;
	public static Touch touch;
	public int maxAliance;
	private Vector2 mousepos;
	private Vector2 worldpos;
	private int alianceCount = 0;
	int x = 0;
	int y = 0;

	public void SetCharacters () {
		if (alianceCount < maxAliance) {

			if (Input.GetMouseButtonUp (0)) {

				mousepos = Input.mousePosition;
				worldpos = Camera.main.ScreenToWorldPoint (mousepos);

				x = (int)Mathf.Round (worldpos.x);
				y = (int)Mathf.Round (worldpos.y);
				for (int n = 0; n < Dungeon.roadPositions.Count; n++) {
					if (new Vector3 (x, y, 0) == Dungeon.roadPositions [n]) {
						GameObject g = Instantiate (Aliance, Dungeon.roadPositions [n], Quaternion.identity) as GameObject;
						AlianceController ac = g.GetComponent<AlianceController> ();
						if (ac != null) {
							StartCoroutine (ac.AlianceMove ());
						}
						alianceCount++;
					}
				}
			}
		}
	}
		
	public bool SetTreasure () {
		if (Treasure == null) {
			if (Input.GetMouseButtonUp (0)) {

				mousepos = Input.mousePosition;
				worldpos = Camera.main.ScreenToWorldPoint (mousepos);

				x = (int)Mathf.Round (worldpos.x);
				y = (int)Mathf.Round (worldpos.y);
				for (int n = 0; n < Dungeon.roadPositions.Count; n++) {
					if (new Vector3 (x, y, 0) == Dungeon.roadPositions [n]) {
						Treasure = Instantiate (treasure, Dungeon.roadPositions [n], Quaternion.identity) as GameObject;
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}
}
